# MSTARaugment
These two notebooks are based on the mnist udacity tests,
they take the SDMS MSTAR dataset and attempt to test the effect of data augmentation on the test score
and convergence time.

## The notebooks

The notebooks expect the user already downloaded the SMDS MSTAR dataset and extracted everything
in the same path as the notebook.

## Results

The following figure shows all validation scores, it gives an insight about the augmentation strategy
effect on the convergence time.

![alt text](graphs/all_valid.png?raw=true "Convergence")

The max validation scores given below are just to validate the test scores results.

![alt text](graphs/max_valid.png?raw=true "Launch Example")

The text scores are given below.

![alt text](graphs/test.png?raw=true "Launch Example")
